WIFI_SSID = "имя точки доступа"
WIFI_PASS = "пароль"
-- Параметры MQTT
MQTT_BrokerIP = "192.168.1.35"
MQTT_BrokerPort = 1883
MQTT_ClientID = "esp-energomera"
MQTT_Client_user = "user"
MQTT_Client_password = "password"
MQTT_TopicString = "/CE102M/"
-- Параметры RS485
RS485_BaudRate = 9600
RS485_DataBits = 7
RS485_ParityBits = uart.PARITY_EVEN
RS485_StopBits = uart.STOPBITS_1
RS485_TxOn_Pin = 6
-- Параметры опроса счётчика
CE102M_Addr = "" -- адрес счётчика ("" или nil для безадресного обмена)
CE102M_CyclePeriod = 10000
CE102M_RequestPeriod = 1000
-- Переменные
local wifi_status_old = 0
local rx_buff = ""
local last_param_name
local MQTT_connected = false
local m
-- Запрашиваемые из счётчика параметры
local ce102m_params = {"VOLTA","CURRE","POWEP","COS_f","FREQU"}
local ce102m_step


-- Настраиваем альтернативные ножки UART (GPIO13, GPIO15)
gpio.mode(7, gpio.INPUT, gpio.PULLUP)
gpio.mode(8, gpio.OUTPUT)
gpio.write(8, 1)
-- Настраиваем ножку управления трансивером RS485
gpio.mode(RS485_TxOn_Pin, gpio.OUTPUT)
gpio.write(RS485_TxOn_Pin, 0)

-- Считаем период передачи 1 байта данных через RS485
--- Биты данных и старт-бит
Bits = RS485_DataBits + 1
--- Бит чётности
if (RS485_ParityBits ~= uart.PARITY_NONE) then Bits = Bits + 1 end
--- Стоп-бит
if (RS485_StopBits == uart.STOPBITS_1) then Bits = Bits + 1
elseif (RS485_StopBits == uart.STOPBITS_1_5) then Bits = Bits + 1.5
else Bits = Bits + 2 end
ByteTransmitTimeUs = RS485_BaudRate / Bits
ByteTransmitTimeUs = 1000000 / ByteTransmitTimeUs

--  Стартуем подключение к wifi-точке
wifi.setmode(wifi.STATION)
wifi.sta.config(WIFI_SSID, WIFI_PASS)
wifi.sta.connect()

-- Функция отправки сообщения по MQTT
function MQTT_send(topic, data)
	if (MQTT_connected) and (m ~= nil) then
		m:publish(topic, data, 0, 0, nil)
	end
end

-- Функция считает BCC (простая сумма всех байт с переполнением) по строке aData
function process_bcc(aData)
    local bcc = 0x00
    for i = 1, #aData do
        bcc = bit.band(0x7F, bcc + string.byte(aData, i))
    end
    return bcc
end

-- Функция-обработчик входящего пакета
function uart_paket_rx()
    -- Информационное сообщение?
    if (string.byte(rx_buff, 1) ~= 0x02) then return end
    -- Контрольная сумма сошлась?
    local bcc = process_bcc(string.sub(rx_buff, 2, #rx_buff - 1))
    if (string.byte(rx_buff, #rx_buff) ~= bcc) then return end
    -- Ищем имя параметра в пакете
    local a = 0
    for rx_buff_one in string.gmatch(rx_buff, last_param_name.."%([0-9]+%.[0-9]+%)") do
		a = a + 1	--увеличиваем счетчик
		
		local str = last_param_name.."%("
		local pos1 = string.find(rx_buff_one, str)
		local pos2 = string.find(rx_buff_one, "%)")
		if (pos1 == nil) and (pos2 == nil) then return end
    
		-- Извлекаем значение и отправляем по MQTT
		pos1 = pos1 + #str - 1
		str = string.sub(rx_buff_one, pos1, pos2 - 1)
		--print(last_param_name.." "..a.." "..str)
		--print(a) 
		if (str ~= "0.00") then
			MQTT_send(MQTT_TopicString..last_param_name..a, str)
		end
    end
end

-- Функция настраивает UART
function uart_setup()
    node.output(function(str) end, 0)
    uart.alt(1)
    uart.setup(0, RS485_BaudRate, RS485_DataBits, RS485_ParityBits, RS485_StopBits, 0)

    uart.on("data", 1, function(data) 
        rx_buff = rx_buff..data
        if (#rx_buff > 256) then rx_buff = "" end
    
        -- [Пере]запускаем таймер ожидания очередного байта
        tmr.alarm(6, 5, tmr.ALARM_SINGLE, function() 
            uart_paket_rx()
        end)
    end, 0)
end

-- Функция отправляет пакет в RS485
function RS485_send(paket)
    gpio.write(RS485_TxOn_Pin, 1)
    uart.write(0, paket)
    tmr.delay(ByteTransmitTimeUs * #paket)
    gpio.write(RS485_TxOn_Pin, 0)
    rx_buff = ""
end

-- Функция отправляет в счётчик запрос на соединение (адресный или безадресный)
function ce102m_start(addr)
	if (addr == nil) then addr = "" end
    RS485_send(string.char(0x2F,0x3F)..addr..string.char(0x21,0x0D,0x0A))
end

-- Функция выдаёт ASK, счётчик отвечает своим номером
function ce102m_p0()
    RS485_send(string.char(0x06,0x30,0x35,0x31,0x0D,0x0A))
end

-- Функция запрашивает у счётчика параметр по мнемонике
function ce102m_get_param(param_name)
	local request = string.char(0x01,0x52,0x31,0x02)..param_name.."()"..string.char(0x03)
	local bcc = process_bcc(string.sub(request, 2, #request))
	request = request..string.char(bcc)
	RS485_send(request)
    last_param_name = param_name
end

-- Функция отправляет в счётчик запрос на разъединение
function ce102m_stop()
	RS485_send(string.char(0x01,0x42,0x30,0x03,0x75))
end

-- Функция отправляет в счётчик очередной запрос
function NextRequest()
    if (ce102m_step == 1) then ce102m_start(CE102M_Addr)
    elseif (ce102m_step == 2) then ce102m_p0()
    elseif (ce102m_step == (#ce102m_params + 3)) then ce102m_stop()
    else ce102m_get_param(ce102m_params[ce102m_step - 2])
    end
end

-- Таймеры опроса счётчика
tmr.alarm(1, CE102M_CyclePeriod, tmr.ALARM_AUTO, function()
    ce102m_step = 1
    
    tmr.alarm(2, CE102M_RequestPeriod, tmr.ALARM_AUTO, function()
        NextRequest(ce102m_step)
        ce102m_step = ce102m_step + 1
        if (ce102m_step > (#ce102m_params + 3)) then
            tmr.unregister(2)
        end
    end)
end)

-- Таймер установки соединения по WiFi и MQTT
tmr.alarm(0, 5000, tmr.ALARM_AUTO, function()
    print("tmr0 "..wifi_status_old.." "..wifi.sta.status())

    if wifi.sta.status() == 5 then -- подключение есть
        if wifi_status_old ~= 5 then -- Произошло подключение к Wifi, IP получен
            print(wifi.sta.getip())
            
			m = mqtt.Client(MQTT_ClientID, 120, MQTT_Client_user, MQTT_Client_password)

            -- Определяем обработчики событий от клиента MQTT
            m:on("connect", function(client) 
			    -- Мы подключились к брокеру
            	MQTT_connected = true 
				uart_setup()
			end)
            m:on("offline", function(client) 
                -- Мы отключились от брокера
				MQTT_connected = false
            end)

            m:connect(MQTT_BrokerIP, MQTT_BrokerPort, 0, 1, nil)
        else
            -- WiFi-подключение есть и не разрывалось
        end
    else
        wifi.sta.connect()
    end

    -- Запоминаем состояние подключения к Wifi для следующего такта таймера
    wifi_status_old = wifi.sta.status()
end)
